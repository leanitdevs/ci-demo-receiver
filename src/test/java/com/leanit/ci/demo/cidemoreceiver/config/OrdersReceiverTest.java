package com.leanit.ci.demo.cidemoreceiver.config;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.leanit.ci.demo.utilitylib.rabbit.IBus;

@RunWith(SpringRunner.class)
public class OrdersReceiverTest {

	@MockBean private IBus bus;
	
	@TestConfiguration
	static class OrdersReceiverServiceContextConfiguration {
		@Bean
		public OrdersReceiverService getOrdersReceiverService() {
			return new OrdersReceiverService();
		}
	}
	
	@Test
	public void test() {
		verify(bus, times(1)).subscribeConsumer(any());
	}
}