package com.leanit.ci.demo.cidemoreceiver.config;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.leanit.ci.demo.utilitylib.environment.DemoEnvironment;

public class ReceiverConfigurationFactoryTest {

	@Test
	public void testConfigForLocalshot() {
		ReceiverConfigurationDto configDto = new ReceiverConfigurationFactory().buildConfiguration(DemoEnvironment.LOCAL);
		
		assertThat(configDto.rabbitConfiguration.host, is("localhost"));
	}

	@Test
	public void testConfigForTomcat() {
		ReceiverConfigurationDto configDto = new ReceiverConfigurationFactory().buildConfiguration(DemoEnvironment.TOMCAT);
		
		assertThat(configDto.rabbitConfiguration.host, is("localhost"));
	}

	@Test
	public void testConfigForK8s() {
		ReceiverConfigurationDto configDto = new ReceiverConfigurationFactory().buildConfiguration(DemoEnvironment.KUBERNETES);
		
		assertThat(configDto.rabbitConfiguration.host, is("rabbitqueue-svc"));
	}
}