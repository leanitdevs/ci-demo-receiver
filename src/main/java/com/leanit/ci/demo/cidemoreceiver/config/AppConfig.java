package com.leanit.ci.demo.cidemoreceiver.config;

import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.leanit.ci.demo.utilitylib.environment.DemoEnvironment;
import com.leanit.ci.demo.utilitylib.environment.EnvironmentDetector;
import com.leanit.ci.demo.utilitylib.rabbit.IBus;
import com.leanit.ci.demo.utilitylib.rabbit.IRabbitBusManager;
import com.leanit.ci.demo.utilitylib.rabbit.RabbitBusManager;

@Configuration
public class AppConfig {

	private IRabbitBusManager busManager;
	private ReceiverConfigurationFactory configurationFactory;
	private EnvironmentDetector environmentDetector;

	public AppConfig () {
		this(new ReceiverConfigurationFactory(), new EnvironmentDetector());
	}
	
	public AppConfig(ReceiverConfigurationFactory configurationFactory, EnvironmentDetector environmentDetector) {
		this.configurationFactory = configurationFactory;
		this.environmentDetector = environmentDetector;
	}
	
	@Bean 
	public IBus getBus() {
		busManager = new RabbitBusManager(this.getConfiguration().rabbitConfiguration);
		return busManager.start();
	}
	
	@Bean
	public DemoEnvironment getCurrentEnvironment() {
		return environmentDetector.detect();
	}
	
	@Bean
	public ReceiverConfigurationDto getConfiguration() {
		return configurationFactory.buildConfiguration(getCurrentEnvironment());
	}
	
	@Bean
	public Gson getGson() {
		return new Gson(); // Customization can be added here
	}
	
	@PreDestroy
	public void destroy() {
		busManager.stop();
	}
}