package com.leanit.ci.demo.cidemoreceiver.config;

import com.leanit.ci.demo.utilitylib.environment.DemoEnvironment;
import com.leanit.common.utilitylib.rabbit.utils.RabbitConfiguration;
import com.leanit.common.utilitylib.rabbit.utils.RabbitConstants;

class ReceiverConfigurationFactory {

	ReceiverConfigurationDto buildConfiguration(DemoEnvironment demoEnv) {
		ReceiverConfigurationDto returnValue = null;

		switch(demoEnv) {
			case LOCAL:
				returnValue = this.doForLocal();
				break;
			case TOMCAT:
				returnValue = this.doForTomcat();
				break;
			case KUBERNETES:
				returnValue = this.doForKubernetes();
				break;
		}
		
		return returnValue;
	}
	
	private ReceiverConfigurationDto doForLocal() {
		return new ReceiverConfigurationDto(new RabbitConfiguration("localhost", 5672, "cidemo", "cidemo123", RabbitConstants.QUEUE_NAME));
	}

	private ReceiverConfigurationDto doForTomcat() {
		return new ReceiverConfigurationDto(new RabbitConfiguration("localhost", 5672, "cidemo", "cidemo123", RabbitConstants.QUEUE_NAME));
	}
	
	private ReceiverConfigurationDto doForKubernetes() {
		return new ReceiverConfigurationDto(new RabbitConfiguration("rabbitqueue-svc", 5672, "guest", "guest", RabbitConstants.QUEUE_NAME));
	}
}