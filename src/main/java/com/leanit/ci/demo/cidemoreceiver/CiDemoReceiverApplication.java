package com.leanit.ci.demo.cidemoreceiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CiDemoReceiverApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CiDemoReceiverApplication.class, args);
	}
}