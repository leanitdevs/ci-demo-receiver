package com.leanit.ci.demo.cidemoreceiver.config;

import com.leanit.common.utilitylib.rabbit.utils.RabbitConfiguration;

public class ReceiverConfigurationDto {
	public RabbitConfiguration rabbitConfiguration;

	public ReceiverConfigurationDto(RabbitConfiguration rabbitConfiguration) {
		this.rabbitConfiguration = rabbitConfiguration;
	}
}
