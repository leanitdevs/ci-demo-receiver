package com.leanit.ci.demo.cidemoreceiver.config;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leanit.ci.demo.utilitylib.domaindto.OrderDto;
import com.leanit.ci.demo.utilitylib.rabbit.IBus;
import com.leanit.ci.demo.utilitylib.rabbit.IConsumer;

@Service
public class OrdersReceiverService {

	@Autowired IBus bus;
	
	private static final Logger log = LoggerFactory.getLogger(OrdersReceiverService.class);

	@PostConstruct
	void init() {
		bus.subscribeConsumer(new IConsumer<OrderDto>() {
			
			@Override
			public void consume(OrderDto orderMsg) {
				log.info(String.format("Consumed  %s", orderMsg.description));
				try {
//					Thread.sleep(2000);
//					Thread.sleep(100);
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			@Override
			public String getConsumerName() {
				return "DummyOrdersConsumer";
			}

			@Override
			public Class<OrderDto> getMsgClass() {
				return OrderDto.class;
			}
		});
	}
}